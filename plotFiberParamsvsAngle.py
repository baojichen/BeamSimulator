#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 16:00:58 2022

@author: Cotton
"""


import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm

directory = '/Volumes/KINGSTON/20220719'

FolderName = []
ExpTime = []
Angle = []
NA = []
NADiff = []
Radius = []
Type = []

# sort by last word
folders = os.listdir(directory)
folders.sort(key=lambda s: s.split('-')[-1])

# collect fiber rotated data
for folder in folders:
    if (folder.startswith('20220719') or folder.startswith('20220718')) and not folder.endswith('csv'): 
        
        FolderName.append(folder)
        
        metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')

        exptime = metadata['ExposureTime'][0] # get first exposure time
        ExpTime.append(exptime)
        
        ang = int(folder.split('-')[-1])
        Angle.append(ang)
        
        df = pd.read_csv(os.path.join(directory, folder, 'theory/NA_y.csv'))
        
        diff = abs(df['G1D_NA'] - df['G2D_NA'])
        err = diff.min()
        index = diff.idxmin()
        
        NAavg = (df['G1D_NA'][index] + df['G2D_NA'][index])/2 # avg NA values
        rad = df['a [um]'][index]
        
        NA.append(NAavg)
        NADiff.append(err)
        Radius.append(rad)
        Type.append('FiberRotated')
        
        #plt.plot(metadata['StagePos'], metadata['ExposureTime'], label=label1, marker='x')
        
# collect image rotated data
for folder in folders:
    if folder.endswith('-0') and folder.startswith('202207'):
        for subfolder in os.listdir(os.path.join(directory, folder)):
            if subfolder.endswith('deg'):
                FolderName.append(folder)
                metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')

                exptime = metadata['ExposureTime'][0] # get first exposure time
                ExpTime.append(exptime)
                
                ang = int(subfolder.split('-')[0])
                Angle.append(ang)
                
                df = pd.read_csv(os.path.join(directory, folder, subfolder, 'theory/NA_y.csv'))
                
                diff = abs(df['G1D_NA'] - df['G2D_NA'])
                err = diff.min()
                index = diff.idxmin()
                
                NAavg = (df['G1D_NA'][index] + df['G2D_NA'][index])/2 # avg NA values
                rad = df['a [um]'][index]
                
                NA.append(NAavg)
                NADiff.append(err)
                Radius.append(rad)
                Type.append('ImageRotated')
            
            
            
data = {'Folder Name': FolderName,
        'Exposure Time': ExpTime,
        'NA': NA,
        'NA Diff': NADiff,
        'Radius': Radius,
        'Angle': Angle,
        'Type':Type
        }
df = pd.DataFrame(data)

fiberrot = df.loc[df['Type']=='FiberRotated']
imagerot = df.loc[df['Type']=='ImageRotated']


# normalize and scale
markersizef = (np.array(fiberrot['NA Diff']) / max(fiberrot['NA Diff']) * 40)**2 + 100
markersizei = (np.array(imagerot['NA Diff']) / max(imagerot['NA Diff']) * 40)**2 + 100

fig, ax = plt.subplots()
ax.set_ylim([0.0975, 0.102]) # must set limits for scaling
M = ax.transData.get_matrix()
yscale = M[1,1]
markersizef = yscale*fiberrot['NA Diff']
markersizef = np.clip(markersizef, 3, None)
markersizei = yscale*imagerot['NA Diff']
markersizei = np.clip(markersizei, 3, None)

#sc = plt.scatter(ExpTime, NA, s=markersize**2 + 100, c=Radius, alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.65)
sc = plt.scatter(fiberrot['Angle'], fiberrot['NA'], s=(markersizef)**2, c=fiberrot['Radius'], alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.7)
sc1 = plt.scatter(imagerot['Angle'], imagerot['NA'], s=markersizei**2, c=imagerot['Radius'], marker='s', alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.7)
#plt.errorbar(fiberrot['Angle'], fiberrot['NA'], fmt='none', yerr=fiberrot['NA Diff']/2, capsize=3, errorevery=3)
# plt.errorbar(ExpTime, Gauss2D, fmt='o', yerr=Gauss2Derr, label='Gauss2D')
# plt.errorbar(ExpTime, Moments, fmt='o', yerr=Momentserr, label='Moments')
plt.colorbar(sc)
plt.text(0, 0.0960, 'markersize = diff between NA \n square = image rotated, circle = fiber rotated ')
#plt.text(14.1, 0.097, 'markersize = diff between NA \n Free rotation')
plt.ylabel('Avg of closest Gauss1D and Gauss2D NA')
plt.xlabel('Angle [deg]')

plt.title('Fiber Parameters vs Angle \n 20220719-P5-405BPM-FC2-PCSide Y Fit')

plt.show()


df.to_csv(os.path.join(directory, "20220719-rotation-y.csv"))















