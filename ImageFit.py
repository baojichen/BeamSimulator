# -*- coding: utf-8 -*-
"""
Created on Tue Nov  9 15:52:44 2021

@author: Florian Egli
"""

import os
import sys
import time
sys.path.append(r'C:\Users\baojichen\Desktop\BeamSimulator\BeamSimulator\fiberanalysis')

import fitFunctions.Analysis as Analysis
import pandas as pd
import numpy as np
import multiprocessing as mp
import fitFunctions.fitMFD as fitMFD


class ImageFitHandler():
    '''
    Class providing functions for the "ImageFitter"-threads.
    '''
    
    def __init__(self, parent=None):
        self._pixelSize = None
        self._wavelength = None
        self._iter = None
        
        self._dataFolder = None
        self._secondMomentsFolder = None
        self._gauss2DFolder = None
        
        self._fileList = []
        self._bkgList = []
        
        print("ImageFitHandler Object created")
        
    @property
    def dataFolder(self):
        return self._dataFolder
    
    @property
    def gauss1DFolder(self):
        return self._gauss1DFolder
    
    @property
    def gauss2DFolder(self):
        return self._gauss2DFolder
    
    @property
    def secondMomentsFolder(self):
        return self._secondMomentsFolder
    
    @property
    def fileList(self):
        return self._fileList
    
    @property
    def bkgList(self):
        return self._bkgList
    
    @property
    def pixelSize(self):
        return self._pixelSize
    
    @property
    def wavelength(self):
        return self._wavelength
        
    @dataFolder.setter
    def dataFolder(self, path):
        '''
        Sets the dataFolder property to path and does the following background jobs:
            - sets the secondMomentsFolder and the gauss2DFolder Attributes
            - creates the secondMoments Folder and the gauss2D Folder if necessary
            - Reads and sorts the pictures and bkg pictures

        Parameters
        ----------
        path : string
            path to dataFolder.
            Must be correct, currently no checks are done on this

        Returns
        -------
        None.

        '''
        
        self._iter = 0
        self._dataFolder = path
        
        self._secondMomentsFolder = path + 'SecondMomentsAnalysis/'
        if not os.path.exists(self._secondMomentsFolder):
            os.makedirs(self._secondMomentsFolder)
            
        self._gauss2DFolder = path + 'Gauss2DAnalysis/'
        if not os.path.exists(self._gauss2DFolder):
            os.makedirs(self._gauss2DFolder)
            
        self._gauss1DFolder = path + 'Gauss1DAnalysis/'
        if not os.path.exists(self._gauss1DFolder):
            os.makedirs(self._gauss1DFolder)
        
        self._fileList = []
        self._bkgList = []
        
        # compile list of image and bkg images
        for file in os.listdir(self._dataFolder):
            if not file.startswith('._'): # some hidden files
                if (file.endswith(".tif") or file.endswith(".tiff")) and not file.startswith("Bkg-"):
                    #if int(file[:3]) > 284 and int(file[:3]) < 301:
                    self._fileList.append(file)
                    #self._bkgList.append("Bkg-"+file+"000")
                    #for dummy bkg image (cinogy and wincam)
                    #self._bkgList.append('Bkg-fake.tif')
                elif file.endswith(".tif") and file.startswith("Bkg-"):
                    #if int(file[4:7]) > 284 and int(file[4:7]) < 301:
                    self._bkgList.append(file)
                    continue
        
        self._fileList.sort(key=lambda f: float(f.split('mm')[0]))
        self._bkgList.sort(key=lambda f: float((f.split("Bkg-")[1]).split("mm")[0]))
        
    @pixelSize.setter
    def pixelSize(self, pixelSize):
        self._pixelSize = pixelSize*1e3 #convert to mm
        
    @wavelength.setter
    def wavelength(self, wavelength):
        self._wavelength = wavelength*1e3 # convert to mm
        
    def AnalyzeFolder(self):
        
        t = time.time()
        
        pool = mp.Pool(1) # set number of threads
        filePairs = [[self.dataFolder+self.fileList[index], self.dataFolder+self.bkgList[index]] for index, file in enumerate(self.fileList, start=0)]
        results = []
        results = pool.map(Analysis.runAnalysis, filePairs) # run actual analysis
        
        dfGauss1D = pd.DataFrame()
        dfGauss2D = pd.DataFrame()
        dfSecondMoments = pd.DataFrame()
        for index, result in enumerate(results):
            filename = self.fileList[index]
            pos = float(self.fileList[index].split('mm')[0])
            meta = {"Filename": filename,
                    "StagePos_mm": pos}
            dfGauss1D = dfGauss1D.append(
                pd.Series({**meta, **result["Gauss1D"]}), ignore_index=True)
            dfGauss2D = dfGauss2D.append(
                pd.Series({**meta, **result["Gauss2D"]}), ignore_index=True)
            dfSecondMoments = dfSecondMoments.append(
                pd.Series({**meta, **result["Moments"]}), ignore_index=True)
        
        dfGauss1D.index.name = "ImageID"
        dfGauss2D.index.name = "ImageID"
        dfSecondMoments.index.name = "ImageID"    
        dfGauss1D.to_csv(self.gauss1DFolder+"analysisGauss1D.csv")
        dfGauss2D.to_csv(self.gauss2DFolder+"analysisGauss2D.csv")
        dfSecondMoments.to_csv(self.secondMomentsFolder+"analysisSecondMoments.csv")
        
        fitMFD.fitMFD(self.dataFolder, self.pixelSize, self.wavelength)
        print(f"Analysis took {np.floor((time.time()-t)/60):.0f} min, {(time.time()-t)%60:.1f} s")

    def AnalyzeFolder1D(self):
        t = time.time()
        
        pool = mp.Pool(1) # set number of threads
        filePairs = [[self.dataFolder+self.fileList[index], self.dataFolder+self.bkgList[index]] for index, file in enumerate(self.fileList, start=0)]
        results = []
        results = pool.map(Analysis.runAnalysis1D, filePairs) # run actual analysis
        
        dfGauss1D = pd.DataFrame()
        
        for index, result in enumerate(results):
            filename = self.fileList[index]
            pos = float(self.fileList[index].split('mm')[0])
            meta = {"Filename": filename,
                    "StagePos_mm": pos}
            dfGauss1D = dfGauss1D.append(
                pd.Series({**meta, **result["Gauss1D"]}), ignore_index=True)
        
        dfGauss1D.index.name = "ImageID"
        
        dfGauss1D.to_csv(self.gauss1DFolder+"analysisGauss1D.csv")
        
        fitMFD.fitMFD1D(self.dataFolder, self.pixelSize, self.wavelength)
        print(f"Analysis took {np.floor((time.time()-t)/60):.0f} min, {(time.time()-t)%60:.1f} s")
        
    def AnalyzeFolder1DAndD4S(self):
        
        t = time.time()
        
        pool = mp.Pool(1) # set number of threads
        filePairs = [[self.dataFolder+self.fileList[index], self.dataFolder+self.bkgList[index]] for index, file in enumerate(self.fileList, start=0)]
        results = []
        #results = pool.map(Analysis.runAnalysis1DAndD4S, filePairs) # run actual analysis
        for pair in filePairs:
            results.append(Analysis.runAnalysis1DAndD4S(pair))
        dfGauss1D = pd.DataFrame()
        dfSecondMoments = pd.DataFrame()
        for index, result in enumerate(results):
            filename = self.fileList[index]
            pos = float(self.fileList[index].split('mm')[0])
            meta = {"Filename": filename,
                    "StagePos_mm": pos}
            dfGauss1D = dfGauss1D.append(
                pd.Series({**meta, **result["Gauss1D"]}), ignore_index=True)
            dfSecondMoments = dfSecondMoments.append(
                pd.Series({**meta, **result["Moments"]}), ignore_index=True)
        
        dfGauss1D.index.name = "ImageID"
        dfSecondMoments.index.name = "ImageID"    
        dfGauss1D.to_csv(self.gauss1DFolder+"analysisGauss1D.csv")
        dfSecondMoments.to_csv(self.secondMomentsFolder+"analysisSecondMoments.csv")
        
        fitMFD.fitMFD1DAndD4S(self.dataFolder, self.pixelSize, self.wavelength)
        print(f"Analysis1DAndD4S took {np.floor((time.time()-t)/60):.0f} min, {(time.time()-t)%60:.1f} s")
        
if __name__ == "__main__":        
    test = ImageFitHandler()
    test.dataFolder = sys.argv[1]+"/"
    
    test.wavelength = 405e-9 #in m
    #test.pixelSize = 5.5e-6 #in m
    test.pixelSize = 2.4e-6 #
    #print(test.fileList)
    t = time.time()
    test.AnalyzeFolder1DAndD4S()
    