#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 16:00:58 2022

@author: Cotton
"""


import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm

directories = ['/Users/Cotton/Downloads/20210726-SameExp']

FolderName = []
ExpTime = []
NA = []
NADiff = []
Radius = []
Type = []


# collect fiber rotated data
for directory in directories:
    # sort by last word
    folders = os.listdir(directory)
    folders.sort(key=lambda s: s.split('-')[-1])
    for folder in folders:
        if folder.startswith('202207') and not folder.endswith('csv'): 
            
            FolderName.append(folder)
            
            metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')
    
            exptime = metadata['ExposureTime'][0] # get first exposure time
            ExpTime.append(exptime)
            
            df = pd.read_csv(os.path.join(directory, folder, 'theory/NA_y.csv'))
            
            diff = abs(df['G1D_NA'] - df['G2D_NA'])
            err = diff.min()
            index = diff.idxmin()
            
            NAavg = (df['G1D_NA'][index] + df['G2D_NA'][index])/2 # avg NA values
            rad = df['a [um]'][index]
            
            NA.append(NAavg)
            NADiff.append(err)
            Radius.append(rad)
            Type.append('SameExp')
            
            #plt.plot(metadata['StagePos'], metadata['ExposureTime'], label=label1, marker='x')

directories = ['/Users/Cotton/Downloads/20210726-AutoExp']

for directory in directories:
    folders = os.listdir(directory)
    folders.sort(key=lambda s: s.split('-')[-1])
    
    # collect image rotated data
    for folder in folders:
        if folder.startswith('202207') and not folder.endswith('csv'):
            FolderName.append(folder)
            metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')
    
            exptime = metadata['ExposureTime'][0] # get first exposure time
            ExpTime.append(exptime)
            
            df = pd.read_csv(os.path.join(directory, folder, 'theory/NA_y.csv'))
            
            diff = abs(df['G1D_NA'] - df['G2D_NA'])
            err = diff.min()
            index = diff.idxmin()
            
            NAavg = (df['G1D_NA'][index] + df['G2D_NA'][index])/2 # avg NA values
            rad = df['a [um]'][index]
            
            NA.append(NAavg)
            NADiff.append(err)
            Radius.append(rad)
            Type.append('AutoExp')
            
            
            
data = {'Folder Name': FolderName,
        'Exposure Time': ExpTime,
        'NA': NA,
        'NA Diff': NADiff,
        'Radius': Radius,
        'Type':Type
        }
df = pd.DataFrame(data)

fiberrot = df.loc[df['Type']=='SameExp']
imagerot = df.loc[df['Type']=='AutoExp']


# normalize and scale
# markersizef = (np.array(fiberrot['NA Diff']) / max(imagerot['NA Diff']) * 40)**2 + 100
# markersizei = (np.array(imagerot['NA Diff']) / max(imagerot['NA Diff']) * 40)**2 + 100

fig, ax = plt.subplots()
ax.set_ylim([0.099, 0.101]) # must set limits for scaling
M = ax.transData.get_matrix()
yscale = M[1,1]
markersizef = yscale*fiberrot['NA Diff']
markersizef = np.clip(markersizef, 4, None)
markersizei = yscale*imagerot['NA Diff']
markersizei = np.clip(markersizei, 4, None)

#sc = plt.scatter(ExpTime, NA, s=markersize**2 + 100, c=Radius, alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.65)
sc = plt.scatter(fiberrot['Exposure Time'], fiberrot['NA'], s=(markersizef/2)**2, c=fiberrot['Radius'], alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.7)
sc1 = plt.scatter(imagerot['Exposure Time'], imagerot['NA'], s=(markersizei/2)**2, c=imagerot['Radius'], marker='s', alpha = 0.5, cmap='winter', vmin=1.45, vmax=1.7)
# plt.errorbar(fiberrot['Exposure Time'], fiberrot['NA'], fmt='none', yerr=fiberrot['NA Diff']/2, capsize=3)
# plt.errorbar(ExpTime, Gauss2D, fmt='o', yerr=Gauss2Derr, label='Gauss2D')
# plt.errorbar(ExpTime, Moments, fmt='o', yerr=Momentserr, label='Moments')
plt.colorbar(sc)
plt.text(9, 0.0985, 'markersize = diff between NA / 2 \n square = auto exp, circle = same exp ')
#plt.text(14.1, 0.097, 'markersize = diff between NA \n Free rotation')
plt.ylabel('Avg of closest Gauss1D and Gauss2D NA')
plt.xlabel('Exposure Time [ms]')

plt.title('Fiber Parameters vs Exp Time \n 20220726-P5-405BPM-FC2-PCSide Y Fit')

plt.show()


df.to_csv(os.path.join(directory, "20220726-sameexp-y.csv"))















