#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 15:37:15 2022

@author: Tian

plots exposure time vs z distance 

"""

import os 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

directory = '/Users/Cotton/Downloads/20220811-PMC400RGB-IDS'

wincam_file = '/Users/Cotton/Downloads/20220816-PMC400RGB-WinCam/20220816-WinCam-PMC-400RGB-1/exposures.txt'
cinogy_file = '/Users/Cotton/Downloads/20220811-PMC400RGB-Cinogy/20220811-PMC-400RGB-1/exposures.txt'

for folder in os.listdir(directory):
    if folder.startswith('20220811-1537-PMC-400RGB-1') and folder.endswith('-1'): 
        # plot exposure times
        metadata = pd.read_csv(os.path.join(directory, folder, 'data.csv'), sep=';')
        label1 = folder.split('-')[-2]
        plt.plot(metadata['StagePos'], metadata['ExposureTime'], label='IDS', marker='o')

wincam = pd.read_csv(wincam_file, sep=',')
cinogy = pd.read_csv(cinogy_file, sep=',')

plt.plot(wincam['StagePos'], wincam['ExposureTime'], label='WinCam', marker='o')
plt.plot(cinogy['StagePos'], cinogy['ExposureTime'], label='Cinogy', marker='o')

plt.xlabel('$z$ [mm]')
plt.ylabel('Exposure Time [ms]')

plt.title('Camera Exposure Times \n 20220811 PMC-400RGB-2.8-NA011-1-APC-150-P')
plt.legend()
plt.show()