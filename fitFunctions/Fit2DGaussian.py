# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:46:18 2015

@author: Lothar
"""

from math import atan
import scipy.optimize
import numpy as np
import scipy.special as special

def EFieldFiberMode(r,a,kT,gamma,s):
    r=abs(r)
    core = lambda x: FiberModeCore(x, kT)
    cladding = lambda x: FiberModeCladding(x, gamma, s)
    return np.piecewise(r, [np.where(r<=a, True, False),np.where(r>a, True, False)], [core, cladding])

def FiberModeCore(r, kT):
    return special.j0(kT*r)

def FiberModeCladding(r, gamma, s):
    return s*special.k0(gamma*r)

def BoundaryConditions(X, V):
    Y = np.sqrt(V**2-X**2)
    #the following has to be zero from boundary conditions
    return X*special.j1(X)/special.j0(X)-Y*special.k1(Y)/special.k0(Y)

def DetermFiberMode(lam, a, NA):
    V=NA*2*np.pi*a/lam
    fun = lambda x: BoundaryConditions(x,V)
    #scipy.optimize.root finds zero of the specified function, ensures boundary conditions
    sol= scipy.optimize.root(fun, 1)
    X=sol.x[0]
    Y=np.sqrt(V**2-X**2) 
    kT=X/a
    gamma=Y/a
    s=special.j0(kT*a)/special.k0(gamma*a)
    return (kT,gamma,s)

# BilligInt means "shabby integral" because this is a very brute force approach
def BilligInt(func,xmin,xmax,N):
    dx=(xmax-xmin)/N
    x=xmin    
    intval=0
    while x <= xmax:
        x+=dx
        intval += func(x)*dx
    return intval


# diffracted E-Field
def EBeug(E0,r,intlimit,z,N,lam):
    if z==0:
        return E0(r)
    k=2*np.pi/lam
    intfunc = lambda x: E0(x)*np.exp(-1j*k*x**2/2/z)*x*special.jv(0,k*x*r/z)
    return 2*np.pi*1j/lam/z*np.exp(-1j*k*r**2/2/z)*BilligInt(intfunc,0,intlimit,N)

def fiberMode(A, lam, z,  c):
    def fiberMode0(r, a, NA,r0):
        r=(r-r0)*2.4e-6
        kT, gamma, s = DetermFiberMode(lam, a, NA)
        E0 = lambda r1: EFieldFiberMode(r1, a, kT, gamma, s)
        EB=EBeug(E0, r,50*a, z, 500, lam)
        EB0=EBeug(E0, 0,50*a, z, 500, lam)
        return np.abs(EB)**2/(np.abs(EB0)**2)*A+c

    return fiberMode0





def gauss1d(r,r0,A,w,c):
    return A*np.exp(-2*(r-r0)**2/w**2)+c

def gauss1dWithFixedAAndC(A,c):
    return lambda r,r0,w:A*np.exp(-2*(r-r0)**2/w**2)+c

def gaussian2D(height, center_x, center_y, width_x, width_y, rotation, offset):
    """Returns a gaussian function with the given parameters"""
    width_x = float(width_x)
    width_y = float(width_y)

    rotation = np.deg2rad(rotation)
    center_x_tmp = center_x * np.cos(rotation) - center_y * np.sin(rotation)
    center_y_tmp = center_x * np.sin(rotation) + center_y * np.cos(rotation)

    center_x=center_x_tmp
    center_y=center_y_tmp        
        
    def rotgauss(x,y):
        xp = x * np.cos(rotation) - y * np.sin(rotation)
        yp = x * np.sin(rotation) + y * np.cos(rotation)
        g = height*np.exp(
            -2*(((center_x-xp)/width_x)**2+
                ((center_y-yp)/width_y)**2)) + offset
        return g
    return rotgauss
        
def gaussian2DRotFixed(height, center_x, center_y, width_x, width_y, offset):
    # rotation angle is a fixed 0 degrees
    return gaussian2D(height, center_x, center_y, width_x, width_y, 0.1, offset)
    #return gaussian2D(height, center_x, center_y, width_x, width_y, 23.58, offset)
    
def moments(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    X = np.arange(0, data.shape[0])
    Y = np.arange(0, data.shape[1])   
    xint = data.sum(axis=0)
    yint = data.sum(axis=1)
    x = np.sum(X*yint)/np.sum(yint)
    y = np.sum(Y*xint)/np.sum(yint)
    col = data[:, int(y)]
    width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())
    row = data[int(x), :]
    width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())
    height = data.max()
    return height, x, y, width_x, width_y, 0.0, 0.0

def momentsRotFixed(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution by calculating its
    moments """
    X = np.arange(0, data.shape[0])
    Y = np.arange(0, data.shape[1])   
    xint = data.sum(axis=0)
    yint = data.sum(axis=1)
    x = np.sum(X*yint)/np.sum(yint)
    y = np.sum(Y*xint)/np.sum(yint)
    col = data[:, int(y)]
    width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())/2.2
    #row = data[int(x), :]
    #width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())/2.2
    height = data.max()
        
    maxrow=data[:,int(y)].argmax()
    maxcol=data[maxrow,:].argmax()
    xvals = np.arange(0,data.shape[1])
    popt, pcov = scipy.optimize.curve_fit(gauss1d, xvals, data[maxrow,:],(x,height,width_x,data[0][0]))
    
    return popt[1]/1.03, maxrow, maxcol,  1.05*popt[2], 1.05*popt[2], popt[3]
    
def fitGaussian2D(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction1 = lambda p: np.ravel(gaussian2D(*p)(*np.indices(data.shape)) - data)
    def errorfunction(p):
        print(p)
        return np.ravel(gaussian2D(*p)(*np.indices(data.shape)) - data)
    #p, pcov, tmp1, tmp2, tmp3 = scipy.optimize.leastsq(errorfunction, params, full_output=True)
    ab=errorfunction(params)
    result = scipy.optimize.least_squares(errorfunction, params, bounds=([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, 0, -np.inf], [np.inf, np.inf, np.inf, np.inf, np.inf, 360, np.inf]))
    p = result.x
    J = result.jac
    pcov = np.linalg.inv(J.T.dot(J))
    
    #calculate errors
    s_sq = (errorfunction(p)**2).sum()/(len(data)-len(p))
    pcov = pcov*s_sq
    error = []
    for i in range(len(p)):
        error.append(np.abs(pcov[i][i])**0.5)
    
    #print("Param:", p)
    #print("Error:", error)
    
    # make rotation angle positive
    x = p[-2]%360
    if x<0: x=x+360
    print("Param:", p)
    #print(x)
    return p, error

def fitGaussian2DRotFixed(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = momentsRotFixed(data)
    errorfunction = lambda p: np.ravel(gaussian2DRotFixed(*p)(*np.indices(data.shape)) - data)
    # p, success = scipy.optimize.leastsq(errorfunction, params)

    result = scipy.optimize.least_squares(errorfunction, params, bounds=([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, -np.inf], [np.inf, np.inf, np.inf, np.inf, np.inf, np.inf]))
    p = result.x
    J = result.jac
    pcov = np.linalg.inv(J.T.dot(J))
    
    #calculate errors
    s_sq = (errorfunction(p)**2).sum()/(len(data)-len(p))
    pcov = pcov*s_sq
    error = []
    for i in range(len(p)):
        error.append(np.abs(pcov[i][i])**0.5)

    return p, error


def FiberMode2DFactory(x0, y0, A, c, z0, lam):
    aaa=0
    def fiberMode0(a, NA, theta, ratio):
        nonlocal aaa
        kT, gamma, s = DetermFiberMode(lam, a, NA)
        E0 = lambda r1: EFieldFiberMode(r1, a, kT, gamma, s)
        EB0=EBeug(E0, 0,50*a, z0, 500, lam)
        def fiberMode1(x,y):
            nonlocal aaa
            
            r=(np.sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0)))
            phi=np.arctan((y-y0)/(x-x0))
            d=r*np.sin(theta-phi)
            e=r*np.cos(theta-phi)
            x=e*np.cos(theta)+d*ratio*np.sin(theta)
            y=e*np.sin(theta)-d*ratio*np.cos(theta)
            r=(np.sqrt(x*x+y*y))
            EB=EBeug(E0, r,50*a, z0, 500, lam)
            val=np.abs(EB)**2/(np.abs(EB0)**2)*A+c
            aaa=aaa+1
            if(aaa%1==0):
                print(aaa)
                print(np.min(r))
                print(np.max(val))
                #print(r)
                #print(val)
                #print(r.shape)
                #rint(EB)
            return val
        return fiberMode1
    return fiberMode0

def FiberMode2DFactory1(data:np.ndarray,gridwidth, theta, ratio, x0, y0, A, c, z0, lam):
    
    return FiberMode2D
        
def fitFiberMode2D(data):
    """Returns (height, x, y, width_x, width_y)
    the gaussian parameters of a 2D distribution found by a fit"""
    params = moments(data)
    errorfunction = lambda p: np.ravel(gaussian2D(*p)(*np.indices(data.shape)) - data)
    #p, pcov, tmp1, tmp2, tmp3 = scipy.optimize.leastsq(errorfunction, params, full_output=True)
    result = scipy.optimize.least_squares(errorfunction, params, bounds=([-np.inf, -np.inf, -np.inf, -np.inf, -np.inf, 0, -np.inf], [np.inf, np.inf, np.inf, np.inf, np.inf, 360, np.inf]))
    p = result.x
    a=p[5]
    long=0
    short=0
    if(p[3]>p[4]):
        a=a%360
        if(a<0):
            a=a+360
        a=a%180
        long=p[3]
        short=p[4]
    else:
        a=a%360
        if(a<0):
            a=a+360
            a=a+90
        a=a%180
        long=p[4]
        short=p[3]
    a=a/360*2*np.pi
    ratio=long/short
    mode=FiberMode2DFactory1(data,2.4e-6,a,ratio,p[1],p[2],p[0],p[6],5e-3,405e-9)
    
    
    result = scipy.optimize.least_squares(mode, (1.5e-6,0.12), bounds=([0.8e-6,0.08], [2.2e-6,0.2]))
    

    J = result.jac
    pcov = np.linalg.inv(J.T.dot(J))
    
    #calculate errors
    s_sq = (errorfunction(p)**2).sum()/(len(data)-len(p))
    pcov = pcov*s_sq
    error = []
    for i in range(len(p)):
        error.append(np.abs(pcov[i][i])**0.5)
    
    #print("Param:", p)
    #print("Error:", error)
    
    # make rotation angle positive
    x = p[-2]%360
    if x<0: x=x+360
    print("Param:", p)
    #print(x)
    return p, error